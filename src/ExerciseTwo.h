#include <Arduino.h>

class ExerciseTwo
{
    private:
        int incomingByte = 0; // For incoming serial data
        int lastByte = 0; // Last received byte

    public:
        void setup();
        void loop();
};