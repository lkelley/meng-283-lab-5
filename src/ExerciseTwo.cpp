/**
 * Lab 5 Exercise 2
 * 
 * 
 * 
 * Written for Arduino Uno
 * By Terence Kelley
 */

#include <Arduino.h>
#include "ExerciseTwo.h"

void ExerciseTwo::setup() {
    pinMode(13, OUTPUT);
	Serial.begin(9600);     // opens serial port, sets data rate to 9600 bps

	Serial.println("Flashing test LED");

	digitalWrite(13, HIGH);
	delay(500);
	digitalWrite(13, LOW);
	delay(500);
	digitalWrite(13, HIGH);
	delay(500);
	digitalWrite(13, LOW);
	delay(500);
	digitalWrite(13, HIGH);
	delay(500);
	digitalWrite(13, LOW);

	Serial.println("Test complete");
}

void ExerciseTwo::loop() {
	// read the incoming byte:
	this->incomingByte = analogRead(A0);

	if (abs((long)this->incomingByte - (long)this->lastByte > 1)) {
		// say what you got:
		Serial.print("Pot value: ");
		Serial.println(incomingByte, DEC);
	}

	this->lastByte = this->incomingByte;
}
