/**
 * Lab 5 Exercise 1
 * 
 * Basic serial communication test platform. Send bytes to the board
 * which will be output to the serial monitor.
 * 
 * Written for Arduino Uno
 * By Terence Kelley
 */

#include <Arduino.h>
#include "ExerciseOne.h"

void ExerciseOne::setup() {
    pinMode(13, OUTPUT);
	Serial.begin(9600);     // opens serial port, sets data rate to 9600 bps

	Serial.println("Flashing test LED");

	digitalWrite(13, HIGH);
	delay(500);
	digitalWrite(13, LOW);
	delay(500);
	digitalWrite(13, HIGH);
	delay(500);
	digitalWrite(13, LOW);
	delay(500);
	digitalWrite(13, HIGH);
	delay(500);
	digitalWrite(13, LOW);

	Serial.println("Test complete");
}

void ExerciseOne::loop() {
    // send data only when you receive data:
	if (Serial.available() > 0) {
		// read the incoming byte:
		incomingByte = Serial.read();

		// say what you got:
		Serial.print("I received: ");
		Serial.println(incomingByte, DEC);
	}
}
