/**
 * MENG 283 Lab 5 Controller
 * 
 * Exercises can be run from here by changing the type of the "exercise" variable
 */

#include <Arduino.h>
#include "ExerciseOne.h"
#include "ExerciseTwo.h"

ExerciseTwo exercise;

void setup() {
	exercise.setup();
}

void loop() {
	exercise.loop();
}
